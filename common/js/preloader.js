$.fn.preload = function (fn) {
    var len = this.length, i = 0;
    return this.each(function () {
        var tmp = new Image(), self = this;
        if (fn) tmp.onload = function () {
            fn.call(self, 100 * ++i / len, i === len);
        };
        tmp.src = this.src;
    });
};

$(toLoad).preload(function(perc, done) {
    //console.log(this, perc, done);
    $('.progress .bar').velocity({
	    width: perc + '%'
    }, 1000, function(){
		if ((done === true) && (window.loaded === true)) {
		    $('.preloader').velocity({
			    opacity: '0'
		    }, 1000, function() {
			    $('.preloader').detach();
		    });
	    }
    });
});

$(window).load(function(){
	window.loaded = true;
	//console.log('window.loaded: ' + window.loaded);
});