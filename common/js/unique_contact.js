$(document).ready(function(){

	var request;

	$('#contact').submit(function(event){

		// abort any pending request
		if (request) {
			request.abort();
		}

		// set local variables
		var $form = $(this);

		// select and cache all fields
		var $inputs = $form.find("input, select, button, textarea");

		// serialise all data
		var serializedData = $form.serialize();

		// disable all inputs during ajax request
		// NOTE: disable AFTER serialization
		// disabled elements will not be serialized
		$inputs.prop("disabled", true);

		// send request to module
		request = $.ajax({
			url: "common/includes/contact_send.mod.php",
			type: "POST",
			data: serializedData
		});

		// request.done(function (response, textStatus, jqXHR){
		// response = stuff from ajax load
		// textStatus = success or fail
		// jqXHR = ajax debug object
		request.done(function (response){
			// actions to take when successful
			// console.log('WHOHOOO! ' + textStatus);
			$(response)
				.hide()
				.prependTo('.contact-form')
				.fadeIn("fast", "swing")
				.delay(10000)
				.fadeOut("fast", "swing", function(){
					$(this).detach();
				});
		});

		request.fail(function (jqXHR, textStatus, errorThrown){
			// actions to take when unsuccessful
			console.error("FAIL! ;_; " + textStatus, errorThrown);
		});

		request.always(function (){
			// actions to take that occur regardless
			$inputs.prop("disabled", false);
			//$('.contact-form .error, .contact-form .success').detach();
		});

		event.preventDefault();

	});

});