$(document).ready(function(){

	$.fn.overlayToggle = function(e) {
		e.preventDefault();
		$('.wrapper').toggleClass('overlay-hidden');
		$('.project-navigation').toggleClass('overlay-displayed');
		$('.controls').toggleClass('overlay-displayed');
	};

	$.fn.scrollProject = function(e, position) {
		$('#image' + position)
			.stop()
			.velocity(
				"scroll", {
					duration: 1000,
					axis: 'x',
					easing: 'easeInOutExpo',
					container: $('.project')
				});
			e.preventDefault();
	};

	if(Modernizr.csstransitions && Modernizr.csstransforms) {
		$('.wrapper').on({
			click: function(e) {
				$(this).overlayToggle(e);
			}
		}, '.control-close, .control-open');
	}

	var hasTouch = 'ontouchstart' in document.documentElement; // check for touchscreen functionality
	var ismobi = navigator.userAgent.match(/Mobi/i); // check for Mobile browser useragent - http://www.abeautifulsite.net/detecting-mobile-devices-with-javascript

	if((hasTouch !== true) && (ismobi === null)) {
		$(function(){
			var position = 0;
			var imgcount = $('.project > img').length;

			//console.log('number of images: ' + imgcount);
			//<a data-position="1" class="paging-steps">Page 02</a>

			if (imgcount > 1) {

				var docFragment = $(document.createDocumentFragment());
				var pagination = '';

				for (var i = 0; i < imgcount; i++) {
					pagination += '<a data-position="' + i + '" class="paging-steps';
					if (i === 0) {
						pagination += ' selected';
					}
					pagination += '">Page ' + (i+1) + '</a>';
				}

				//for (var i = 0; i < imgcount; i++) {
				//	var className = '';
				//	if (i === 0) {
				//		className = 'paging-steps selected';
				//	} else {
				//		className = "paging-steps";
				//	}
				//	pagination += $("<a />", {
				//		"data-position": i,
				//		"text": "Page" + (i+1),
				//		"class": className
				//	});
				//}

				docFragment.append('<a class="project-navigation back">Previous Image</a><a class="project-navigation forward">Next Image</a><div class="project-navigation paging">' + pagination + '</div>');

				$('.project').after(docFragment);

				$('.paging-steps').bind('click', function(e){
					var prevPage = $('.paging-steps.selected');
					position = $(this).attr('data-position');
					prevPage.removeClass('selected');
					$(this)
						.addClass('selected')
						.scrollProject(e, position);

				});

				$('.project-navigation.forward').bind('click', function(e){
					var prevPage = $('.paging-steps.selected');

					if (position < (imgcount-1)) {
						position++;
						prevPage
							.removeClass('selected')
							.next()
							.addClass('selected');
						$(this)
							.scrollProject(e, position);
					}
				});

				$('.project-navigation.back').bind('click', function(e){
					var prevPage = $('.paging-steps.selected');

					if (position > 0) {
						position--;
						prevPage
							.removeClass('selected')
							.prev()
							.addClass('selected');
						$(this)
							.scrollProject(e, position);
					}
				});
			}
		});
	} else {
		$('.project').addClass('overflow-on');
	}
});