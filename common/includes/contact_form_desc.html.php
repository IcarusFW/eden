<div class="contact-content">
	<h2>Send a message</h2>
     <p>Feel free to use the form below to send any enquiries about the work that I do and can provide. I will endeavour to reply as quickly as I can.</p>
	<div class="contact-form">
    	<?php include $incs . 'contact_form.html.php'; ?>
    </div>
</div>