<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/common/includes/helpers.inc.php'; ?>
<?php if (isset($feedback)) { echo $feedback; } ?>
<form id="contact" name="contact" action="?sendmsg" method="post">
    <fieldset>
    <legend>Contact me:</legend>
    	<dl>
    		<dt>
    			<label class="form-label">Name:</label>
    		</dt>
			<dd>
				<input type="text" name="name" size="20" <?php echo (isset($name)) ? 'value="' . $name . '" ' : '' ?>/>
			</dd>
			<dt>
				<label class="form-label">E-mail Address:</label>
			</dt>
			<dd>
				<input type="text" name="email" size="20" <?php echo (isset($mail)) ? 'value="' . $mail . '" ' : '' ?>/>
			</dd>
			<dt>
				<label class="form-label">Subject:</label>
			</dt>
			<dd>
				<input type="text" name="subject" size="20" <?php echo (isset($subject)) ? 'value="' . $subject . '" ' : '' ?>/>
			</dd>
			<dt>
				<label class="form-label">Message:</label>
			</dt>
			<dd>
				<textarea name="message"><?php echo (isset($msg)) ? $msg : '' ?></textarea>
			</dd>
			<dt class="no-label">
				<label class="form-label">&nbsp;</label>
			</dt>
			<dd>
				<input type="submit" id="submit" name="submit" value="Send Message" />
			</dd>
    	</dl>
    </fieldset>
</form>