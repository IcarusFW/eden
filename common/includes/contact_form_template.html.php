<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8" />
	<!-- meta for enforcing IE versions: "edge" attempts to use the latest rendering engine available in the browser -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Eden Network</title>
	<meta name="description" content="Eden Network - professional portfolio of UI/UX design graduate Simon Coong" />
	<meta name="keywords" content="" />
	<meta name="author" content="Simon Coong" />
	<meta name="robots" content="" />

	<!-- meta tag for responsive resizing, required to make media queries work on devices -->
	<!-- include "maximum-scale=1" within 'content' parameters if you wish to turn off device zooming -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- modernizr.js -->
	<!-- <script src="common/bower_components/modernizr/modernizr.js"></script> -->
	<script src="common/js/modernizr.custom.js"></script> <!-- css-calc version -->

	<!-- stylesheets -->
	<link rel="stylesheet" href="common/bower_components/normalize-css/normalize.css" /> <!-- Normalize ver1.1.3 for IE7+ compatibility -->
	<link rel="stylesheet" href="common/css/common.css" /> <!-- common sitewide stylesheet -->
	<link rel="stylesheet" href="common/css/structure.css" /> <!-- common structural stylesheet -->
	<link rel="stylesheet" href="common/css/unique_contact.css" /> <!-- profile page stylesheet -->
</head>

<body>

	<div class="header">
		<div class="block-title">
			<div class="title-wrapper">
				<span class="title">Eden Network</span>
				<span class="subtitle">Simon Coong, UI/UX Designer &amp; Developer</span>
				<a href="#menu" class="menu showmenu">Show Menu</a>
			</div>
		</div><div id="menu" class="block-navigation hidden">
			<a href="#hidemenu" class="menu hidemenu">Hide Menu</a>
			<ul>
				<li><a class="nav-folio" href="index.html">Portfolio</a></li>
				<li><a class="nav-profile" href="profile.html">Profile</a></li>
				<li><a class="nav-blog" href="http://simon.webdesignfutures.co.uk">Journal</a></li>
				<li><a class="nav-photo" href="http://viewfinder.thestudio-uk.com/">Photography</a></li>
				<li><a class="nav-contact selected" href="contact.php">Contact</a></li>
			</ul>
		</div>
	</div> <!-- .header -->

	<div class="wrapper">

		<div class="container single contact-image">
			<img src="gallery/_contact.jpg" alt="Photo taken at Shift 2012" />
		</div>

		<div class="container double contact-info">
			<?php include $incs . 'contact_form_desc.html.php'; ?>
		</div>

	</div>

	<!-- end-loaded javascripts -->
	<script src="common/bower_components/jquery/dist/jquery.min.js"></script> <!-- jQuery 1.10.2 production framework -->
	<script src="common/js/navigation.js"></script> <!-- header toggle script -->
	<script src="common/js/unique_contact.js"></script> <!-- contact form scripts -->
</body>
</html>