<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Error Output</title>
<style type="text/css">
.errorout {
	color: #fff;
	border: 1px solid #F00;
	padding: 10px;
	margin-top: 10px;
	-moz-border-radius: 5px;
	-khtml-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	background-color: #a00;
	font-family: Georgia, "Times New Roman", serif;
	font-size: 20px;
}
.center {
	top: 50%;
	left: 0px;
	position: absolute;
	width: 100%;
	height: 1px;
	overflow: visible;
}
.container {
	position: absolute;
	width: 600px;
	height: 150px;
	margin-left: -300px;
	left: 50%;
	top: -75px;
}
</style>
</head>

<body>
<div class="center">
	<div class="container">
	<div class="errorout"><?php echo $_SESSION['output']; unset($_SESSION['output']); ?></div>
    </div>
</div>
</body>
</html>