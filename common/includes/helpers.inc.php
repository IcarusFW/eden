<?php
// document relative variables
$root = $_SERVER['DOCUMENT_ROOT'];
$incs = $root . '/common/includes/';
$mods = $root . '/common/includes/modules/';

// set variables for most common code includes
$connect = $root . $incs . 'db.inc.php';
$mquotes = $root . $incs . 'magicquotes.inc.php';
$errorout = $root . $incs . 'error.html.php';

// html tag-converting function
function html($value) {
	return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}
?>