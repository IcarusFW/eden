<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>GET, POST and REQUEST Output List</title>
</head>

<body>
	<center>
		<h3>Displaying all form data:</h3>
		<p>Here is all the form data passed to the dump template:</p>
		<?php
        foreach ($_REQUEST as $index => $value) {
            if (is_array($value)) {
                foreach ($value as $number => $item) {
                    echo "${index} [${number}] => $item <br>";	
                }
            } else {
                echo $index . " => " . $value . "<br>";	
            }
        }
        ?>
	</center>
</body>
</html>