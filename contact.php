<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/common/includes/helpers.inc.php';
include_once $incs . 'magicquotes.inc.php';

if(isset($_GET['sendmsg'])) {

	// grab all POST values and assign to variables
	$name = $_POST['name'];
	$mail = $_POST['email'];
	$subject = $_POST['subject'];
	$msg = $_POST['message'];

	// set an empty error-catch variable
	$errors = '';

	if (empty($name)) {
		$errors .= '<li>Your name is required.</li>';
	}
	if (empty($mail)) {
		$errors .= '<li>A contact e-mail address is required.</li>';
	}
	if (empty($subject)) {
		$errors .= '<li>A message subject is required.</li>';
	}
	if (empty($msg)) {
		$errors .= '<li>The message content should not be blank.</li>';
	}

	// if there are errors, reload the form and display error messages
	if ($errors != '') {

		$feedback = '<div class="error">There are some problems with your composed message:<ul>' . $errors . '</ul></div>';

		$name = $name;
		$mail = $mail;
		$subject = $subject;
		$msg = $msg;

		include $incs . 'contact_form_template.html.php';
		//include $incs . 'formdump.html.php';
		exit();

	} else {
		// ... otherwise send the booking e-mail and load the confirmation message
		$message = "Name: ".$name.
		"\r\nE-mail: ".$mail.
		"\r\nSubject: ".$subject.
		"\r\n\nMessage: ".$msg;

		// echo $message;

		$to = 'me@thestudio-uk.com';
		$subj = $subject;
		$headers = 'From: me@thestudio-uk.com' . "\r\n" .
			'Reply-To: me@thestudio-uk.com' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();

		mail($to, $subj, $message, $headers);

		$feedback = '<div class="success"><h2>Thank you</h2><p>You message has been sent successfully. I will reply as soon as I can.</p></div>';
		include $incs . 'contact_form_template.html.php';
		//include $incs . 'formdump.html.php';
		//header('LOCATION: ' . $_SERVER['PHP_SELF']);
		exit();
	}
}

include $incs . 'contact_form_template.html.php';

?>